import { FormUtil } from '@/utils/form.util';
import RequestUtil from '@/utils/request.util';

const ConversationApi = {
  searchConversation(_q: string) {
    const filter = { _q };
    return RequestUtil.axios.get(FormUtil.endpointWithQuery(`/conversations/search?filter=${JSON.stringify(filter)}`));
  },
  async updateConversation(id?: string, data?: any) {
    return RequestUtil.axios.put(`/conversations/${id}`, data);
  },
  getListConversation() {
    return RequestUtil.axios.get('/conversations');
  },
  getListMessage(_id: string, skip = 0, limit = 30) {
    return RequestUtil.axios.get(`/conversations/${_id}/messages?skip=${skip}&limit=${limit}`);
  },
  createGroupConversation(groupConversation: any) {
    return RequestUtil.axios.post('/conversations/group', groupConversation);
  },
};
export default ConversationApi;
