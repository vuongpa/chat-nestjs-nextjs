import React, { useEffect, useMemo, useState } from 'react';
import {
  Layout, Typography, Avatar, Button, Badge, Space,
} from 'antd';
import { useRouter } from 'next/router';
import { useDispatch, useSelector } from 'react-redux';

import styles from './Home.module.scss';

import UserSettings from '@/features/user-settings/UserSettings';
import FormModal from '@/components/form-modal/FormModal';
import { useCurrentUser } from '@/features/auth/authHooks';
import CommonUtil from '@/utils/common.util';
import { AuthUtils } from '@/utils/auth.util';
import { authActions } from '@/features/auth/authSlice';
import { chatActions, chatSelector } from '@/features/chat/chatSlice';
import WithNoSsr from '@/components/WithSSR';
import ChatNav from '@/features/chat/chat-nav/ChatNav';
import ChatOnboard from '@/features/chat/ChatOnboard';
import { RootState } from '@/app/store';

const { Header, Content } = Layout;

export default function Home() {
  const [showUserSetting, setShowUserSetting] = useState(false);
  const currentUser = useCurrentUser();
  const fullName = useMemo(() => CommonUtil.getFullName(currentUser), [currentUser]);
  const { push } = useRouter();
  const dispatch = useDispatch();
  const socketConnected = useSelector(chatSelector.socketConnected);
  const chatStore = useSelector((state: RootState) => state.chat);

  const logout = () => {
    dispatch(authActions.logout());
  };

  useEffect(() => {
    if (!chatStore.loaded) {
      dispatch(chatActions.requestListConversation());
    }

    if (!AuthUtils.getRefreshToken()) {
      push('/login');
    }
  }, []);

  return (
    <>
      <Layout className={styles['main-layout']}>
        <Header className={styles['header-wrapper']}>
          <div className={styles.header}>
            <Space>
              <WithNoSsr>
                <Badge dot={socketConnected}>
                  <Avatar src={currentUser?.avatar_url} shape="square" onClick={() => setShowUserSetting(true)}>
                    {fullName?.[0] || 'U'}
                  </Avatar>
                </Badge>
              </WithNoSsr>
              <Typography.Title style={{ marginBottom: 0 }} level={5}>{fullName}</Typography.Title>
            </Space>
          </div>
          <Button type="text" onClick={logout}>
            <Typography.Text underline>
              Đăng xuất
            </Typography.Text>
          </Button>
        </Header>
        <Content>
          <Layout className={styles['main-content']}>
            <Layout.Sider width={300} className={styles.sider} theme="light">
              <ChatNav />
            </Layout.Sider>
            <Layout>
              <ChatOnboard />
            </Layout>
          </Layout>
        </Content>
      </Layout>
      <FormModal {...{
        open: showUserSetting,
        destroyOnClose: true,
        onCancel: () => setShowUserSetting(false),
        title: 'Cài đặt cá nhân',
        cancelText: 'Hủy',
        okText: 'Cập nhật',
      }}
      >
        <UserSettings />
      </FormModal>
    </>
  );
}
