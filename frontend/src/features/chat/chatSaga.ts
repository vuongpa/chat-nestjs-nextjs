import {
  call, cancel, fork, put, take, takeEvery,
} from 'redux-saga/effects';
import { Socket } from 'socket.io-client';
import { Task } from 'redux-saga';

import { authActions } from '../auth/authSlice';

import { connectSocket } from './connectSocket';
import { sendMessageTask } from './sendMessage.task';
import { receiveMessageTask } from './receiveMessage.task';
import { chatActions } from './chatSlice';
import requestListConversation from './requestListConversation.task';
import requestListMessage from './requestListMessage.task';
import createConversation from './createConversation.task';
import createGroupConversation from './createGroupConversation.task';

import { AuthUtils } from '@/utils/auth.util';

function* handleIO(socket: Socket) {
  yield fork(sendMessageTask, socket);
  yield fork(receiveMessageTask, socket);
}

function* chatFlow() {
  try {
    const socket: Socket = yield call(connectSocket);
    const task: Task = yield fork(handleIO, socket);

    yield put(chatActions.setSocketConnected(true));

    yield take(authActions.logout);
    yield cancel(task);
  } catch (err) {
    yield put(chatActions.setSocketConnected(false));
  }
}

function* watchChatFlow() {
  while (true) {
    const isLoggedIn = Boolean(AuthUtils.getAccessToken());

    yield takeEvery(chatActions.requestListConversation, requestListConversation);
    yield takeEvery(chatActions.requestListMessage, requestListMessage);
    yield takeEvery(chatActions.createConversation, createConversation);
    yield takeEvery(chatActions.createGroupConversation, createGroupConversation);

    if (isLoggedIn) {
      yield call(chatFlow);
    } else {
      yield take(authActions.loginSuccess.type);
      yield call(chatFlow);
    }
  }
}

export default function* chatSaga() {
  yield fork(watchChatFlow);
}
