import {
  Button, Col, Form, Input, Row,
} from 'antd';
import { useContext } from 'react';
import { useDispatch } from 'react-redux';

import FullNameFormItem from './FullNameFormItem';

import { FormModalContext } from '@/components/form-modal/Context';
import UploadImage from '@/components/upload/UploadImage';
import UserApi from '@/api/UserApi';
import { User } from '@/models/userModel';
import { useCurrentUser } from '@/features/auth/authHooks';
import { authActions } from '@/features/auth/authSlice';

export default function UpdateUserInformation() {
  const { submitRef } = useContext(FormModalContext);
  const currentUser = useCurrentUser();
  const dispatch = useDispatch();

  const updateUser = (formValue: User) => {
    dispatch(authActions.updateUser({ _id: currentUser?._id, formValue }));
  };

  return (
    <Form
      name="updateUserInfo"
      onFinish={updateUser}
      scrollToFirstError
      layout="vertical"
      initialValues={currentUser}
    >
      <Form.Item style={{ display: 'flex', justifyContent: 'center' }} name="avatar_url">
        <UploadImage />
      </Form.Item>
      <FullNameFormItem />
      <Row gutter={[8, 8]}>
        <Col span={12}>
          <Form.Item
            name="phone"
            rules={[{
              pattern: /(84|0[3|5|7|8|9])+([0-9]{8})\b/g,
              message: 'Vui lòng nhập SĐT chính xác',
            }]}
          >
            <Input placeholder="Số điện thoại" />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item name="email">
            <Input disabled placeholder="Email" />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item name="preferences">
        <Input.TextArea placeholder="Sở thích" />
      </Form.Item>
      <Button ref={submitRef} style={{ display: 'none' }} type="primary" htmlType="submit" />
    </Form>
  );
}
