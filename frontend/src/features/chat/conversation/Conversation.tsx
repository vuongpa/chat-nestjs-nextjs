import classNames from 'classnames';
import { Avatar, Typography } from 'antd';
import moment from 'moment';
import React, { useEffect, useMemo, useState } from 'react';
import { UserOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';

import { useActiveConversation } from '../chatHooks';
import { chatActions, chatSelector } from '../chatSlice';

import styles from './Styles.module.scss';

import CommonUtil from '@/utils/common.util';
import { IMessage } from '@/models/messagesModel';
import { useCurrentUser } from '@/features/auth/authHooks';

interface Props {
  name: string;
  avatarUrl?: string;
  lastMessage: IMessage;
  _id: string;
}

const Conversation: React.FC<Props> = React.memo(({
  name,
  avatarUrl,
  lastMessage,
  _id,
}) => {
// Todo: menu options like: block, delete conversation
  const [mouseEnter, setMouseEnter] = useState<boolean>(false);
  const dispatch = useDispatch();
  const activeConversation = useActiveConversation();
  const isSelected = activeConversation?._id === lastMessage?.conversation_id;
  const currentUser = useCurrentUser();
  const listConversation = useSelector(chatSelector.listConversation);
  const conversation = useMemo(() => listConversation.find((c) => c._id === _id), [listConversation]);

  const onMouseEnter = () => {
    setMouseEnter(true);
  };

  const onMouseLeave = () => {
    setMouseEnter(false);
  };

  const handleActiveConversation = () => {
    if (activeConversation?._id === _id) {
      return;
    }

    dispatch(chatActions.activeConversation({
      conversationId: _id,
      active: true,
    }));

    if (conversation?.loaded) {
      return;
    }

    dispatch(chatActions.requestListMessage({ conversation_id: _id }));
  };

  const renderSenderName = () => {
    if (lastMessage?.sender_id?._id === currentUser?._id) {
      return 'Bạn';
    }

    return CommonUtil.getFullName(lastMessage.sender_id);
  };

  useEffect(() => () => setMouseEnter(false), []);

  return (
    <div
      className={classNames(styles.conversation, {
        [styles.selected]: isSelected,
      })}
      onMouseLeave={onMouseLeave}
      onMouseEnter={onMouseEnter}
      onClick={handleActiveConversation}
      onKeyDown={handleActiveConversation}
    >
      <Avatar icon={<UserOutlined />} className={styles.avatar} size="large" src={avatarUrl} />
      <div className={styles['preview-conversation']}>
        <Typography.Paragraph className={styles.name}>{name}</Typography.Paragraph>
        <Typography.Paragraph className={styles.message} type="secondary">{`${renderSenderName()}: ${lastMessage.message}`}</Typography.Paragraph>
        <Typography.Text type="secondary" className={styles.time}>
          {moment(lastMessage?.createdAt).format('DD/MM')}
        </Typography.Text>
      </div>
    </div>
  );
});

export default Conversation;
