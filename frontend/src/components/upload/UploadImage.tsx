import React, { ReactNode, useEffect, useState } from 'react';
import { PlusOutlined } from '@ant-design/icons';
import { Upload } from 'antd';
import type { UploadProps } from 'antd/es/upload';
import type { UploadFile } from 'antd/es/upload/interface';

import { AuthUtils } from '@/utils/auth.util';

interface UploadImageProp extends UploadProps {
  value?: any;
  onChange?: (value?: any) => void;
  uploadButton?: ReactNode | string;
}

export default function UploadImage({
  value, onChange, uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  ), ...restProps
}: UploadImageProp) {
  const [fileList, setFileList] = useState<UploadFile[]>([]);

  const onListChange = (info: any) => {
    setFileList(info.fileList);
    onChange?.(info.fileList?.[0]?.response?.file_url);
  };

  useEffect(() => {
    if (value) {
      setFileList([{ url: value, uid: Math.random().toString(), name: value }]);
    }
  }, []);

  return (
    <Upload
      listType="picture-circle"
      {...restProps}
      action={`${process.env.NEXT_PUBLIC_API_DOMAIN}/files/upload`}
      headers={{ Authorization: `Bearer ${AuthUtils.getAccessToken()}` }}
      fileList={fileList}
      onChange={onListChange}
      multiple={false}
      showUploadList
    >
      {!fileList.length && uploadButton}
    </Upload>
  );
}
