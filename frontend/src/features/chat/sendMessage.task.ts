import { Socket } from 'socket.io-client';
import { take } from 'redux-saga/effects';

import { chatActions } from './chatSlice';

export function* sendMessageTask(socket: Socket) {
  while (true) {
    const { payload } = yield take(chatActions.sendMessage.type);
    socket.emit('send_message', payload);
  }
}
