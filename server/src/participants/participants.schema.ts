import {
  AsyncModelFactory,
  Prop,
  Schema,
  SchemaFactory,
  getConnectionToken,
} from '@nestjs/mongoose';
import mongoose, { Connection } from 'mongoose';
import { Conversation } from 'src/conversation/conversation.schema';
import { User } from 'src/users/users.schema';
import { CommonUtils } from 'utils/common';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ObjectId = require('mongodb').ObjectId;
@Schema({ timestamps: true })
export class Participant {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Conversation.name,
  })
  conversation_id: Conversation;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  users_id: User;

  @Prop()
  type: string;

  @Prop()
  status: string;

  static TYPE = {
    // TODO
  };

  static STATUS = {
    // TODO
    DELETED: 'deleted',
  };
}

const ParticipantSchema = SchemaFactory.createForClass(Participant);
export const participantFactory: AsyncModelFactory = {
  name: Participant.name,
  useFactory: (connection: Connection) => {
    ParticipantSchema.pre<Participant>('save', async function () {
      const [conversation, user] = await Promise.all([
        connection.db.collection('conversations').findOne(
          {
            _id: new ObjectId(this.conversation_id),
          },
          {
            projection: {
              key_search: 1,
            },
          },
        ),
        connection.db.collection('users').findOne(
          {
            _id: new ObjectId(this.users_id),
          },
          {
            projection: {
              first_name: 1,
              middle_name: 1,
              last_name: 1,
            },
          },
        ),
      ]);
      console.log('user:::', user);
      const keySearchArr = conversation?.key_search?.split(',') || [];

      if (this.status !== Participant.STATUS.DELETED) {
        const keySearch = [...keySearchArr, CommonUtils.getFullName(user)];
        connection.db.collection('conversations').findOneAndUpdate(
          {
            _id: new ObjectId(this.conversation_id),
          },
          { $set: { key_search: keySearch.filter(Boolean).join(',') } },
          { upsert: true },
        );
      } else {
        const keySearch = (conversation?.key_search || '').replace(
          CommonUtils.getFullName(user),
          '',
        );
        connection.db.collection('conversations').updateOne(
          {
            _id: new ObjectId(this.conversation_id),
          },
          {
            key_search: keySearch,
          },
        );
      }
    });

    return ParticipantSchema;
  },
  inject: [getConnectionToken()],
};
