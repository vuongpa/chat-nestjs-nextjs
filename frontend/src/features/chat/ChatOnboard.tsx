/* eslint-disable jsx-a11y/alt-text */
import { Layout, Typography } from 'antd';

import styles from './Styles.module.scss';
import { useActiveConversation } from './chatHooks';
import ChatOnboardHeader from './chat-onboard/ChatOnboardHeader';
import ChatOnboardMain from './chat-onboard/ChatOnboardMain';

import WithNoSsr from '@/components/WithSSR';

function DefaultChatOnboard() {
  return (
    <div className={styles.welcome}>
      <Typography.Title level={3}>Chào mừng đến với trò chuyện</Typography.Title>
      <Typography.Text>Khám phá những tiện ích hỗ trợ làm việc và trò chuyện cùng người thân, bạn bè được tối ưu hoá cho máy tính của bạn.</Typography.Text>
      <br />
      <br />
      <br />
      <WithNoSsr>
        <img className={styles['img-welcome']} src="https://chat.zalo.me/assets/quick-message-onboard.3950179c175f636e91e3169b65d1b3e2.png" />
      </WithNoSsr>
    </div>
  );
}

export default function ChatOnboard() {
  const activeConversation = useActiveConversation();

  if (activeConversation) {
    return (
      <Layout.Content className={styles.content}>
        <ChatOnboardHeader />
        <ChatOnboardMain />
      </Layout.Content>
    );
  }

  return (
    <div>
      <DefaultChatOnboard />
    </div>
  );
}
