import { Module } from '@nestjs/common';
import { MessageGateway } from './message/message.gateway';
import { MessageGatewayService } from './message/message_gateway.service';
import { JwtModule } from '@nestjs/jwt';
import { UsersModule } from 'src/users/users.module';
import { MessagesModule } from 'src/messages/messages.module';
import { ConversationModule } from 'src/conversation/conversation.module';
import { ParticipantsModule } from 'src/participants/participants.module';
import { MongooseModule } from '@nestjs/mongoose';
import { deviceFactory } from 'src/devices/devices.schema';
import { ConfigModule } from '@nestjs/config';
import { conversationFactory } from 'src/conversation/conversation.schema';
import { messageFactory } from 'src/messages/messages.schema';
import { participantFactory } from 'src/participants/participants.schema';

@Module({
  imports: [
    JwtModule,
    UsersModule,
    MessagesModule,
    ConversationModule,
    ParticipantsModule,
    ConfigModule,
    MongooseModule.forFeature([deviceFactory, conversationFactory]),
    MongooseModule.forFeatureAsync([messageFactory, participantFactory]),
  ],
  providers: [MessageGateway, MessageGatewayService],
})
export class GateWayModule {}
