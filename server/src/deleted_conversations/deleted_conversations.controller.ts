import { Controller } from '@nestjs/common';
import { DeletedConversationsService } from './deleted_conversations.service';

@Controller('deleted-conversations')
export class DeletedConversationsController {
  constructor(private readonly deletedConversationsService: DeletedConversationsService) {}
}
