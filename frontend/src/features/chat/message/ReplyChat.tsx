import React from 'react';
import { EnterOutlined } from '@ant-design/icons';
import { Button } from 'antd';

interface ReplyChatProps extends Omit<React.HTMLAttributes<HTMLDivElement>, 'ref'> {
  className?: string;
}

export default function ReplyChat(props: ReplyChatProps) {
  return (
    <div {...props}>
      <Button style={{ backgroundColor: '#ededed70' }} type="text" icon={<EnterOutlined />} />
    </div>
  );
}
