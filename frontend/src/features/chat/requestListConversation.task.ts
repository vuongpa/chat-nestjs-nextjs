import { call, put } from 'redux-saga/effects';

import { chatActions } from './chatSlice';

import ConversationApi from '@/api/ConversationApi';
import { Conversation } from '@/models/conversationModel';

export default function* requestListConversation() {
  try {
    const listConversation: Conversation[] = yield call(ConversationApi.getListConversation);
    yield put(chatActions.requestListConversationSuccess(listConversation));
  } catch (err: any) {
    yield put(chatActions.requestListConversationFail(err?.response?.data?.message || ''));
  }
}
