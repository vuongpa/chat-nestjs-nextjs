import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AttachmentsModule } from './attachments/attachments.module';
import { DeletedMessagesModule } from './deleted_messages/deleted_messages.module';
import { DeletedConversationsModule } from './deleted_conversations/deleted_conversations.module';
import { ReportsModule } from './reports/reports.module';
import { MessagesModule } from './messages/messages.module';
import { ConversationModule } from './conversation/conversation.module';
import { ParticipantsModule } from './participants/participants.module';
import { UserVerificationModule } from './user_verification/user_verification.module';
import { DevicesModule } from './devices/devices.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { FilesModule } from './files/files.module';
import { AuthModule } from './auth/auth.module';
import { GateWayModule } from './gatewaies/gateway.module';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get('MONGO_URI'),
      }),
      inject: [ConfigService],
    }),
    ConfigModule.forRoot(),
    UsersModule,
    DevicesModule,
    UserVerificationModule,
    ParticipantsModule,
    ConversationModule,
    MessagesModule,
    ReportsModule,
    DeletedConversationsModule,
    DeletedMessagesModule,
    AttachmentsModule,
    FilesModule,
    AuthModule,
    GateWayModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
