import { Module } from '@nestjs/common';
import { ConversationService } from './conversation.service';
import { ConversationController } from './conversation.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { conversationFactory } from './conversation.schema';
import { messageFactory } from 'src/messages/messages.schema';
import { participantFactory } from 'src/participants/participants.schema';
import { UserFactory } from 'src/users/users.schema';

@Module({
  imports: [
    MongooseModule.forFeature([conversationFactory, UserFactory]),
    MongooseModule.forFeatureAsync([messageFactory, participantFactory]),
  ],
  controllers: [ConversationController],
  providers: [ConversationService],
  exports: [ConversationService],
})
export class ConversationModule {}
