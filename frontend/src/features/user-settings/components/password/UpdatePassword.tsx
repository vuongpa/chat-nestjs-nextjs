import {
  Button, Form, Input, notification,
} from 'antd';
import { useContext } from 'react';

import { FormModalContext } from '@/components/form-modal/Context';
import AuthApi from '@/api/AuthApi';

export default function UpdatePassword() {
  const { submitRef } = useContext(FormModalContext);
  const updatePassword = async (formValue: any) => {
    try {
      await AuthApi.updatePassword(formValue);
      notification.success({ message: '', description: 'Cập nhập mật khẩu thành công' });
    } catch (error: any) {
      notification.error({ message: '', description: error.response.data.message });
    }
  };

  return (
    <Form
      name="resetPassword"
      onFinish={updatePassword}
      layout="vertical"
    >
      <Form.Item
        name="old_password"
        rules={[
          {
            required: true,
            message: 'Vui lòng nhập mật khẩu cũ',
          },
          {
            min: 6,
            message: 'Mật khẩu phải có ít nhất 6 kí tự',
          },
        ]}
      >
        <Input.Password placeholder="* Mật khẩu cũ" />
      </Form.Item>
      <Form.Item
        name="new_password"
        dependencies={['old_password']}
        rules={[
          {
            required: true,
            message: 'Vui lòng nhập mật khẩu mới',
          },
          {
            min: 6,
            message: 'Mật khẩu phải có ít nhất 6 kí tự',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (getFieldValue('old_password') !== value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('Trùng mật khẩu cũ'));
            },
          }),
        ]}
      >
        <Input.Password placeholder="* Mật khẩu mới" />
      </Form.Item>
      <Form.Item
        name="repeat_new_password"
        dependencies={['new_password']}
        rules={[
          {
            required: true,
            message: 'Vui lòng xác nhận mật khẩu!',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('new_password') === value) {
                return Promise.resolve();
              }
              return Promise.reject(new Error('Mật khẩu xác nhận không chính xác'));
            },
          }),
        ]}
      >
        <Input.Password placeholder="* Xác nhận mật khẩu mới" />
      </Form.Item>
      <Button ref={submitRef} type="primary" htmlType="submit" style={{ display: 'none' }} />
    </Form>
  );
}
