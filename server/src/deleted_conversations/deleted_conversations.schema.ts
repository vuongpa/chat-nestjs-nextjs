import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Conversation } from 'src/conversation/conversation.schema';
import { User } from 'src/users/users.schema';

@Schema({ timestamps: true })
export class DeletedConversation {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Conversation.name,
  })
  conversation_id: Conversation;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  users_id: User;
}

export const deletedConversationFactory: ModelDefinition = {
  name: DeletedConversation.name,
  schema: SchemaFactory.createForClass(DeletedConversation),
};
