import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument } from 'mongoose';
import { getMaxListeners } from 'process';
import { User } from 'src/users/users.schema';

export type FileDocument = HydratedDocument<File>;

@Schema({ timestamps: true })
export class File {
  @Prop({
    required: true,
  })
  file_url: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  create_by: User;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  size: number;
}
const FileSchema = SchemaFactory.createForClass(File);
export const FileFactory: ModelDefinition = {
  name: File.name,
  schema: FileSchema,
};
