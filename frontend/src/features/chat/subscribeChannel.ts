import { eventChannel } from 'redux-saga';
import { Socket } from 'socket.io-client';

import { chatActions } from './chatSlice';

import { IMessage } from '@/models/messagesModel';

export default function subscribeChannel(socket: Socket) {
  return eventChannel((emit) => {
    const handleReceiveMessage = (message: IMessage) => {
      emit(chatActions.sendMessageSuccess(message));
    };

    const handleSocketDisconnect = () => {
      emit(chatActions.setSocketConnected(false));
    };

    socket.on('receive_message', handleReceiveMessage);
    socket.on('disconnect', handleSocketDisconnect);

    return () => {
      socket.off('receive_message', handleReceiveMessage);
      socket.off('disconnect', handleSocketDisconnect);
    };
  });
}
