/* eslint-disable camelcase */
import { isEmpty } from 'lodash';

import { User } from '@/models/userModel';

const CommonUtil = {
  getFullName(user?: User) {
    if (isEmpty(user)) return null;

    const { first_name, middle_name, last_name } = user;

    return [first_name, middle_name, last_name].join(' ');
  },
};
export default CommonUtil;
