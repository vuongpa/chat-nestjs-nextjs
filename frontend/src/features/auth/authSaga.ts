import { PayloadAction } from '@reduxjs/toolkit';
import { push } from 'connected-next-router';
import {
  put, takeLatest, takeEvery, call,
} from 'redux-saga/effects';

import { authActions } from './authSlice';

import { JwtPayload, LoginPayload, SignupPayload } from '@/models/authModel';
import AuthApi from '@/api/AuthApi';
import RequestUtil from '@/utils/request.util';
import { User } from '@/models/userModel';
import UserApi from '@/api/UserApi';

function* handleFetchCurrentUser() {
  try {
    const currentUser: User = yield call(RequestUtil.axios.get, '/auth/me');
    yield put(authActions.setCurrentUser(currentUser));
  } catch (err) {
    console.log('fetch current user error: ', err);
  }
}

function* handleUpdateUser({ payload }: PayloadAction<any>) {
  try {
    const userUpdated: User = yield call(UserApi.updateUser, payload._id, payload.formValue);
    yield put(authActions.setCurrentUser(userUpdated));
  } catch (err) {
    console.log('update error: ', err);
  }
}

function* handleLogin(action: PayloadAction<LoginPayload>) {
  try {
    const loginResponse: JwtPayload = yield call(AuthApi.login, action.payload);

    yield put(authActions.loginSuccess(loginResponse));

    yield put(push('/'));
  } catch (error: any) {
    yield put(authActions.loginFail(error.response.data.message));
  }
}

function* handleLogout() {
  yield put(push('/login'));
}

function* handleSignup(action: PayloadAction<SignupPayload>) {
  try {
    const loginResponse: JwtPayload = yield call(AuthApi.signup, action.payload);

    yield put(authActions.loginSuccess(loginResponse));

    yield put(push('/'));
  } catch (error: any) {
    yield put(authActions.loginFail(error.response.data.message));
  }
}

export default function* authSaga() {
  yield takeLatest(authActions.logout, handleLogout);
  yield takeEvery(authActions.login, handleLogin);
  yield takeEvery(authActions.signup, handleSignup);
  yield takeEvery(authActions.fetchCurrentUser, handleFetchCurrentUser);
  yield takeEvery(authActions.updateUser, handleUpdateUser);
}
