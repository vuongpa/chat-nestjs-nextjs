import { Conversation } from './conversationModel';
import { User } from './userModel';

export interface IMessage {
  _id?: string;
  conversation_id: string;
  conversation?: Conversation;
  sender_id: User;
  message_type: 'text'|'image'|'other';
  message: string;
  createdAt?: string;
  updatedAt?: string;
  sending: boolean;
  fakeConversation?: any;
  fakeConversationId?: string;
  fakeId?: string;
}

export const MESSAGE_TYPE = {
  IMAGE: 'image',
  TEXT: 'text',
  OTHER: 'other',
};
