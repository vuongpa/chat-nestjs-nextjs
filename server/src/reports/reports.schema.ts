import {
  DefinitionsFactory,
  ModelDefinition,
  Prop,
  Schema,
  SchemaFactory,
} from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Participant } from 'src/participants/participants.schema';
import { User } from 'src/users/users.schema';

@Schema({ timestamps: true })
export class Report {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Participant.name,
  })
  participants_id: Participant;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  users_id: User;

  @Prop()
  report_type: string;

  @Prop()
  notes: string;

  @Prop()
  status: string;

  static TYPE = {
    //TODO
  };

  static STATUS = {
    //TODO
  };
}

export const reportFactory: ModelDefinition = {
  name: Report.name,
  schema: SchemaFactory.createForClass(Report),
};
