export interface User {
  _id?: string;
  createAt?: string;
  updatedAt?: string;
  password?: string;
  first_name: string;
  last_name: string;
  middle_name: string;
  phone?: string;
  email: string;
  is_active?: boolean;
  is_reported?: boolean;
  is_blocked?: boolean;
  preferences?: string;
  avatar_url?: string;
}
