import {
  CameraOutlined, SearchOutlined, UserOutlined, UsergroupAddOutlined,
} from '@ant-design/icons';
import {
  Avatar,
  Button, Checkbox, Divider, Empty, Form, Input, Modal, Typography,
} from 'antd';
import { useState } from 'react';
import { debounce } from 'lodash';
import { useDispatch } from 'react-redux';

import { chatActions } from '../chatSlice';

import styles from './Styles.module.scss';

import UploadImage from '@/components/upload/UploadImage';
import UserApi from '@/api/UserApi';
import CommonUtil from '@/utils/common.util';
import { User } from '@/models/userModel';
import { useCurrentUser } from '@/features/auth/authHooks';

export default function CreateGroupConversation() {
  const [showGroupConversation, setShowGroupConversation] = useState(false);
  const [searchUsersResult, setSearchUserResult] = useState([]);
  const dispatch = useDispatch();

  const [groupForm] = Form.useForm();
  const [searchForm] = Form.useForm();
  const [selectMemberForm] = Form.useForm();

  const currentUser = useCurrentUser();

  const selectedMember = Form.useWatch('select_member', selectMemberForm);

  const handleSearchUser = debounce(async () => {
    try {
      const keySearch = searchForm.getFieldValue('key_search');
      if (!keySearch) {
        throw new Error('');
      }

      const response: any = await UserApi.searchUser(keySearch);
      setSearchUserResult(response || []);
    } catch (err) {
      setSearchUserResult([]);
    }
  }, 300);

  const createGroup = () => {
    // TODO: Search các tên người khác nhau phải giữ lại được kết quả chọn cũ, tạo một mảng lưu và hiển thị sang bên phải
    if (currentUser?._id) {
      dispatch(chatActions.createGroupConversation({
        title: groupForm.getFieldValue('title'),
        avatar_url: groupForm.getFieldValue('avatar_url'),
        users: [...selectedMember, currentUser._id],
        creator_id: currentUser._id,
      }));
    }
    setShowGroupConversation(false);
  };

  const handleChangeSelect = () => {
    // TODO
  };

  return (
    <>
      <Button type="text" onClick={() => setShowGroupConversation(true)}>
        <UsergroupAddOutlined />
      </Button>
      <Modal
        open={showGroupConversation}
        onCancel={() => { setShowGroupConversation(false); }}
        destroyOnClose
        onOk={createGroup}
        okText="Tạo nhóm"
        cancelText="Hủy"
        title="Tạo nhóm"
        width={520}
      >
        <div className={styles.header}>
          <Form layout="inline" form={groupForm} name="upload_avatar">
            <Form.Item name="avatar_url">
              <UploadImage className={styles['upload-button']} uploadButton={<CameraOutlined />} />
            </Form.Item>
            <Form.Item required className={styles['form-item-title']} name="title">
              <Input className={styles['input-title']} placeholder="Nhập tên nhóm" />
            </Form.Item>
          </Form>
        </div>
        <br />
        <div className={styles.content}>
          <Form form={searchForm} layout="vertical" name="search_member">
            <Form.Item name="key_search" label="Thêm bạn vào nhóm">
              <Input onChange={handleSearchUser} prefix={<SearchOutlined />} placeholder="Nhập tên, số điện thoại, email" />
            </Form.Item>
          </Form>
          <Divider />
          {searchUsersResult.length ? (
            <>
              <Typography.Paragraph strong>
                {`Đã chọn: ${selectedMember?.length || 0}/${searchUsersResult.length} người`}
              </Typography.Paragraph>
              <Form name="select_member" form={selectMemberForm}>
                <Form.Item name="select_member">
                  <Checkbox.Group
                    style={{ display: 'flex', flexDirection: 'column', gap: 15 }}
                    options={searchUsersResult.map((user: User) => {
                      if (!user?._id) {
                        return null;
                      }

                      return {
                        label: (
                          <div>
                            <Avatar style={{ marginRight: 5 }} icon={<UserOutlined />} src={user?.avatar_url} />
                            <Typography.Text>{CommonUtil.getFullName(user)}</Typography.Text>
                          </div>
                        ),
                        value: user._id,
                      };
                    }) as any[]}
                    onChange={handleChangeSelect}
                  />
                </Form.Item>
              </Form>
            </>
          ) : <Empty />}
        </div>
      </Modal>
    </>
  );
}
