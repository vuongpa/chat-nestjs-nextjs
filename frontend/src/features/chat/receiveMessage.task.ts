import { EventChannel } from 'redux-saga';
import { Socket } from 'socket.io-client';
import { call, put, take } from 'redux-saga/effects';
import { Action } from '@reduxjs/toolkit';

import subscribeChannel from './subscribeChannel';

export function* receiveMessageTask(socket: Socket) {
  const socketChannel: EventChannel<Socket> = yield call(subscribeChannel, socket);

  while (true) {
    const action: Action = yield take(socketChannel);
    yield put(action);
  }
}
