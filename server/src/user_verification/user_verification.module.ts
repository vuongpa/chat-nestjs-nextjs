import { Module } from '@nestjs/common';
import { UserVerificationService } from './user_verification.service';
import { UserVerificationController } from './user_verification.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { userVerificationFactory } from './user_verification.schema';

@Module({
  controllers: [UserVerificationController],
  providers: [UserVerificationService],
  imports: [MongooseModule.forFeature([userVerificationFactory])],
})
export class UserVerificationModule {}
