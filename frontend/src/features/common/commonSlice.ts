import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { RootState } from '@/app/store';
import { Conversation } from '@/models/conversationModel';

interface CommonState {
  searchingConversation: boolean;
  searchingResultConversation: Conversation[];
}
const initialState: CommonState = {
  searchingConversation: false,
  searchingResultConversation: [],
};

const commonSlice = createSlice({
  name: 'common',
  initialState,
  reducers: {
    setSearchingConversation(state, { payload }: PayloadAction<boolean>) {
      state.searchingConversation = payload;
    },
    setSearchingResultConversation(state, { payload }: PayloadAction<Conversation[]>) {
      state.searchingResultConversation = payload;
    },
  },
});

export const commonStoreName = commonSlice.name;
export const commonActions = commonSlice.actions;

export const commonSelector = {
  searchingConversation: (state: RootState) => state.common.searchingConversation,
  searchingResultConversation: (state: RootState) => state.common.searchingResultConversation,
};

const commonReducers = commonSlice.reducer;
export default commonReducers;
