import { Avatar, Typography } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { useState } from 'react';

import styles from './Styles.module.scss';
import { AddFriendAction, SendMessageAction } from './ActionWithFriend';

import CommonUtil from '@/utils/common.util';
import { User } from '@/models/userModel';

interface ResultItemSearchUserProps {
  user: User;
}

export default function ResultItemSearchUser({ user }: ResultItemSearchUserProps) {
  const [mouseEnter, setMouseEnter] = useState<boolean>(false);

  return (
    <div
      onMouseEnter={() => setMouseEnter(true)}
      onMouseLeave={() => setMouseEnter(false)}
      className={styles['contact-item']}
    >
      <div className={styles.user}>
        <Avatar icon={<UserOutlined />} src={user?.avatar_url} />
        <div>
          <Typography.Text>
            {CommonUtil.getFullName(user)}
          </Typography.Text>
          <Typography.Paragraph style={{ marginBottom: 0 }} type="secondary">{user?.email}</Typography.Paragraph>
        </div>
      </div>
      {mouseEnter && (
      <div className={styles['contact-action']}>
        <SendMessageAction user={user} />
        <AddFriendAction />
      </div>
      )}
    </div>
  );
}
