import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Message } from 'src/messages/messages.schema';
import { Participant } from 'src/participants/participants.schema';

@Injectable()
export class ConversationService {
  constructor(
    @InjectModel(Message.name)
    private messageModel: Model<Message>,
    @InjectModel(Participant.name)
    private participantModel: Model<Participant>,
  ) {}

  async findConversationWithLastMessage(
    conversationIds: any[],
    skip = 0,
    limit = 15,
  ) {
    console.log('conversationIds:::', conversationIds);
    const messagesByConversationAgg = await this.messageModel.aggregate([
      {
        $match: {
          conversation_id: { $in: conversationIds },
        },
      },
      { $sort: { message_id: -1 } },
      {
        $lookup: {
          from: 'conversations',
          localField: 'conversation_id',
          foreignField: '_id',
          as: 'conversation_id',
        },
      },
      {
        $unwind: '$conversation_id',
      },
      {
        $lookup: {
          from: 'users',
          as: 'sender_id',
          let: { sender_id: '$sender_id' },
          pipeline: [
            {
              $match: {
                $expr: {
                  $eq: ['$_id', '$$sender_id'],
                },
              },
            },
            {
              $project: {
                first_name: 1,
                middle_name: 1,
                last_name: 1,
                email: 1,
                phone: 1,
                avatar_url: 1,
              },
            },
          ],
        },
      },
      { $unwind: '$sender_id' },
      {
        $group: {
          _id: '$conversation_id',
          doc: { $first: '$$ROOT' },
        },
      },
      { $replaceRoot: { newRoot: '$doc' } },
    ]);

    const participantsByConversationIds = await this.participantModel.find(
      {
        conversation_id: {
          $in: conversationIds,
        },
      },
      null,
      {
        populate: {
          path: 'users_id',
          select: {
            first_name: 1,
            middle_name: 1,
            last_name: 1,
            email: 1,
            phone: 1,
            avatar_url: 1,
          },
        },
      },
    );

    return messagesByConversationAgg
      .map((message) => ({
        ...message.conversation_id,
        messages: [
          {
            ...message,
            conversation_id: message.conversation_id._id,
          },
        ],
        users: participantsByConversationIds
          .filter(
            (p) =>
              p.conversation_id.toString() ===
              message.conversation_id._id.toString(),
          )
          .map((p) => p.users_id),
      }))
      .sort((a, b) => b.messages[0].message_id - a.messages[0].message_id);
  }
}
