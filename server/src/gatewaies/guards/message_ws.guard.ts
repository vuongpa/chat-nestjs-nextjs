import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Socket } from 'socket.io';
import { JwtPayload } from 'src/auth/strategies/accessToken.strategy';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class MessageWSGuard implements CanActivate {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const client: Socket = context.switchToWs().getClient<Socket>();
    const accessToken = client.handshake.headers.authorization;

    try {
      const { username: email } = await this.jwtService.verify(accessToken, {
        secret: this.configService.get('JWT_ACCESS_SECRET'),
      });
      const user = await this.userService.findByEmail(email);
      context.switchToWs().getData().currentUser = user;
      return Boolean(user);
    } catch (ex) {
      console.log('web guard: ', ex);
      return false;
    }
  }

  validateUser(payload: JwtPayload): any {
    return payload;
  }
}
