import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './users.schema';
import { Model } from 'mongoose';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { omit } from 'lodash';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ObjectId = require('mongodb').ObjectId;

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name)
    private usersModel: Model<User>,
    private jwtService: JwtService,
    private configService: ConfigService,
  ) {}

  async findByEmail(email: string) {
    return this.usersModel.findOne({ email });
  }

  async decodeAccessToken(accessToken: string) {
    const { username: email, sub } = await this.jwtService.verify(accessToken, {
      secret: this.configService.get('JWT_ACCESS_SECRET'),
    });

    const currentUser = await this.usersModel
      .findOne({
        _id: new ObjectId(sub),
        email,
      })
      .lean();

    return omit(currentUser, 'password');
  }
}
