import { isEmpty } from 'lodash';

export const CommonUtils = {
  jsonValid: (json: string) =>
    /^[\],:{}\s]*$/.test(
      json
        .replace(/\\["\\\/bfnrtu]/g, '@')
        .replace(
          /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
          ']',
        )
        .replace(/(?:^|:|,)(?:\s*\[)+/g, ''),
    ),
  getFullName(user?: any) {
    if (isEmpty(user)) return '';

    const { first_name, middle_name, last_name } = user;

    return [first_name, middle_name, last_name].join(' ');
  },
};
