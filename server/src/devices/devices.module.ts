import { Module } from '@nestjs/common';
import { DevicesService } from './devices.service';
import { DevicesController } from './devices.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { deviceFactory } from './devices.schema';

@Module({
  imports: [MongooseModule.forFeature([deviceFactory])],
  controllers: [DevicesController],
  providers: [DevicesService],
})
export class DevicesModule {}
