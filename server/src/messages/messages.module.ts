import { Module } from '@nestjs/common';
import { MessagesService } from './messages.service';
import { MessagesController } from './messages.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { messageFactory } from './messages.schema';
import { conversationFactory } from 'src/conversation/conversation.schema';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([messageFactory]),
    MongooseModule.forFeature([conversationFactory]),
  ],
  controllers: [MessagesController],
  providers: [MessagesService],
  exports: [MessagesService],
})
export class MessagesModule {}
