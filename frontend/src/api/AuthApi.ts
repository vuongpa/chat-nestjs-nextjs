import { LoginPayload, SignupPayload } from '@/models/authModel';
import RequestUtil from '@/utils/request.util';

const AuthApi = {
  async login(data: LoginPayload) {
    const loginResponse = await RequestUtil.axios.post('/auth/login', data);
    return loginResponse;
  },
  signup(data: SignupPayload) {
    return RequestUtil.axios.post('/auth/signup', data);
  },
  async updatePassword(data: {old_password: string, new_password: string, repeat_new_password: string}) {
    await RequestUtil.axios.put('auth/update-password', data);
  },
};
export default AuthApi;
