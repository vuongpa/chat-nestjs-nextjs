import { Button } from 'antd';
import { useDispatch } from 'react-redux';
import { PlusOutlined, SendOutlined } from '@ant-design/icons';

import { chatActions } from '../chatSlice';

import { User } from '@/models/userModel';
import { useCurrentUser } from '@/features/auth/authHooks';

export function AddFriendAction() {
  return (
    <Button type="text">
      <PlusOutlined />
    </Button>
  );
}

export function SendMessageAction({ user }: {user: User}) {
  const dispatch = useDispatch();
  const currentUser = useCurrentUser();

  const sendMessage = () => {
    dispatch(chatActions.setShowAddFriendModal(false));

    if (currentUser && user) {
      dispatch(chatActions.createConversation({ currentUser, user }));
    }
    /*
    Tìm conversation của hai currentUser và bạn.
    => Y: active conversation => requestMessage
       N: Tạo conversation fake, sau đó sendMessage >> sendMessageSuccess thì update lại.
    */
  };

  return (
    <Button type="text" onClick={sendMessage}>
      <SendOutlined />
    </Button>
  );
}
