import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as bcryptjs from 'bcryptjs';
import { User } from 'src/users/users.schema';
import { LoginDto, SignupDto, UpdatePassword } from './auth.dto';
import { omit } from 'lodash';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name)
    private readonly userModel: Model<User>,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  async updatePassword(
    request,
    { old_password, new_password }: UpdatePassword,
  ) {
    const user = await this.userModel.findById(request.user.sub);
    const isPasswordMatching = await bcryptjs.compare(
      old_password,
      user.password,
    );

    if (!isPasswordMatching) {
      throw new BadRequestException('Mật khẩu không chính xác');
    }

    const newPassword = await bcryptjs.hash(new_password, 10);
    await this.userModel.findByIdAndUpdate(request.user.sub, {
      password: newPassword,
    });
  }

  async signup({ password, repeat_password, ...newUser }: SignupDto) {
    const passwordHash = await bcryptjs.hash(password, 10);
    const user = await this.userModel.create({
      password: passwordHash,
      ...newUser,
    });

    const jwtTokens = await this.generateNewJwtTokens(user.id, newUser.email);
    const userSave = await this.userModel.findById(user.id).lean();

    return {
      ...this.generateJwtPayload(jwtTokens.accessToken, jwtTokens.refreshToken),
      currentUser: omit(userSave, 'password'),
    };
  }

  generateJwtPayload(accessToken: string, refreshToken: string) {
    const expiresIn = this.configService.get('JWT_ACCESS_EXPIRES');
    return {
      accessToken,
      refreshToken,
      expiresIn,
      expiresAt: (new Date().getTime() + expiresIn * 1000).toString(),
    };
  }

  async generateNewJwtTokens(userId: string, email: string) {
    const [accessToken, refreshToken] = await Promise.all([
      this.jwtService.signAsync(
        {
          username: email,
          sub: userId,
        },
        {
          secret: this.configService.get('JWT_ACCESS_SECRET'),
          expiresIn: `${this.configService.get('JWT_ACCESS_EXPIRES')}s`,
        },
      ),
      this.jwtService.signAsync(
        {
          username: email,
          sub: userId,
        },
        {
          secret: this.configService.get('JWT_REFRESH_SECRET'),
          expiresIn: `${this.configService.get('JWT_REFRESH_EXPIRES')}s`,
        },
      ),
    ]);

    return { accessToken, refreshToken };
  }

  async refreshJwtTokens(userId: string) {
    const user = await this.userModel.findById(userId).lean();
    const jwtTokens = await this.generateNewJwtTokens(userId, user.email);
    return {
      ...this.generateJwtPayload(jwtTokens.accessToken, jwtTokens.refreshToken),
      currentUser: omit(user, 'password'),
    };
  }

  async getCurrentUser(userId: string) {
    return this.userModel.findById(userId);
  }

  async login({ email, password }: LoginDto) {
    const user: any = await this.userModel.findOne({ email }).lean();
    if (!user) {
      throw new NotFoundException('Email không chính xác');
    }

    const isPasswordMatching = await bcryptjs.compare(password, user.password);
    if (!isPasswordMatching) {
      throw new BadRequestException('Mật khẩu không chính xác');
    }
    const jwtTokens = await this.generateNewJwtTokens(user._id, user.email);
    const tokens = this.generateJwtPayload(
      jwtTokens.accessToken,
      jwtTokens.refreshToken,
    );

    return {
      ...tokens,
      currentUser: omit(user, 'password'),
    };
  }
}
