import { PayloadAction } from '@reduxjs/toolkit';
import { call, put, select } from 'redux-saga/effects';

import { authSelector } from '../auth/authSlice';

import { chatActions } from './chatSlice';

import { Conversation } from '@/models/conversationModel';
import ConversationApi from '@/api/ConversationApi';
import CommonUtil from '@/utils/common.util';
import { User } from '@/models/userModel';
import { MESSAGE_TYPE } from '@/models/messagesModel';

export default function* createGroupConversation({ payload }: PayloadAction<Conversation>) {
  try {
    const newConversation: Conversation = yield call(ConversationApi.createGroupConversation, payload);
    if (newConversation?._id) {
      yield put(chatActions.createGroupConversationSuccess({ ...newConversation, messages: [] }));
      yield put(chatActions.activeConversation({ conversationId: newConversation._id, active: true }));

      const currentUser: User = yield select(authSelector.currentUser);
      yield put(chatActions.sendMessage({
        conversation_id: newConversation._id,
        message: `${CommonUtil.getFullName(currentUser)} đã tạo cuộc hội thoại`,
        sender_id: currentUser,
        message_type: MESSAGE_TYPE.TEXT,
        sending: true,
      } as any));
    }
  } catch (err) {
    console.log(err);
  }
}
