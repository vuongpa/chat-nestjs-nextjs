/* eslint-disable @typescript-eslint/no-empty-function */
import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { notification } from 'antd';

import { User } from '@/models/userModel';
import { AuthUtils } from '@/utils/auth.util';
import {
  JwtPayload, LoginPayload, SignupPayload,
} from '@/models/authModel';
import { RootState } from '@/app/store';

interface AuthState {
  isLoggedIn: boolean;
  logging: boolean;
  currentUser?: User;
  error: string;
}

const initialState: AuthState = {
  isLoggedIn: false,
  logging: false,
  error: '',
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setCurrentUser(state, action: PayloadAction<User>) {
      state.currentUser = action.payload;
      state.isLoggedIn = true;
      state.error = '';
      state.logging = false;
    },

    login(state, action: PayloadAction<LoginPayload>) {
      state.logging = true;
    },

    loginSuccess(state, action: PayloadAction<JwtPayload>) {
      state.logging = false;
      state.isLoggedIn = true;
      state.error = '';

      const { currentUser, ...tokens } = action.payload;
      state.currentUser = currentUser;
      AuthUtils.setAuthToken(tokens);
    },

    loginFail(state, action: PayloadAction<string>) {
      state.logging = false;
      state.isLoggedIn = false;
      state.error = action.payload;

      notification.error({ message: '', description: action.payload });
      AuthUtils.removeAllToken();
    },

    logout(state) {
      state.isLoggedIn = false;
      state.currentUser = undefined;
      AuthUtils.removeAllToken();
    },

    signup(state, action: PayloadAction<SignupPayload>) {
      state.logging = true;
    },

    fetchCurrentUser() {},
    updateUser(state, action: PayloadAction<{_id?: string, formValue?: User}>) {},
  },
});

const authReducer = authSlice.reducer;
export default authReducer;

export const authStoreName = authSlice.name;

export const authActions = authSlice.actions;

export const authSelector = {
  isLoggedIn: (state: RootState) => state.auth.isLoggedIn,
  logging: (state: RootState) => state.auth.logging,
  currentUser: (state: RootState) => state.auth.currentUser,
  error: (state: RootState) => state.auth.error,
};
