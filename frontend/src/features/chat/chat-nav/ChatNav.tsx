import { Layout } from 'antd';
import { useSelector } from 'react-redux';

import SearchConversation, { CloseSearchConversation } from '../search-conversation/SearchConversation';
import AddFriend from '../add-friend/AddFriend';
import ListConversation from '../list-conversation/ListConversation';
import CreateGroupConversation from '../create-group-conversation/CreateGroupConversation';

import styles from './Styles.module.scss';

import { commonSelector } from '@/features/common/commonSlice';

export default function ChatNav() {
  const searchingConversation = useSelector(commonSelector.searchingConversation);
  return (
    <Layout>
      <Layout.Header className={styles['site-layout-background']}>
        <SearchConversation />
        {searchingConversation && <CloseSearchConversation />}
        {!searchingConversation && (
          <>
            <AddFriend />
            <CreateGroupConversation />
          </>
        )}
      </Layout.Header>
      <Layout.Content>
        <ListConversation />
      </Layout.Content>
    </Layout>
  );
}
