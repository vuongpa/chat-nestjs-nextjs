import {
  Body,
  Controller,
  Get,
  Param,
  Put,
  Query,
  UseGuards,
  Request,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { AccessTokenGuard } from 'src/auth/guards/accessToken.guard';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './users.schema';
import { Model } from 'mongoose';
import { omit } from 'lodash';
import { CommonUtils } from 'utils/common';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const ObjectId = require('mongodb').ObjectId;

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    @InjectModel(User.name)
    private usersModel: Model<User>,
  ) {}

  @UseGuards(AccessTokenGuard)
  @Get('search')
  async searchUser(@Query() query, @Request() request) {
    const currentUserId = request.user.sub;
    const { filter: filterJson = '{}', skip = 0, limit = 10 } = query || {};

    const filter = CommonUtils.jsonValid(filterJson)
      ? JSON.parse(filterJson)
      : filterJson;
    const _q = filter?._q;
    const normalizeFilter = omit(filter, '_q');

    const searchResult = await this.usersModel
      .find(
        {
          ...normalizeFilter,
          _id: { $ne: new ObjectId(currentUserId) },
          $text: { $search: _q },
        },
        {
          score: { $meta: 'textScore' },
          avatar_url: 1,
          email: 1,
          first_name: 1,
          last_name: 1,
          middle_name: 1,
          phone: 1,
        },
        { skip, limit, sort: { score: { $meta: 'textScore' } } },
      )
      .lean();

    return searchResult;
  }

  @UseGuards(AccessTokenGuard)
  @Put(':id')
  async update(@Param('id') id: string, @Body() requestUpdate) {
    try {
      await this.usersModel.updateOne({ _id: id }, requestUpdate);
      return this.usersModel.findById(id);
    } catch (error) {
      console.log(error);
    }
  }
}
