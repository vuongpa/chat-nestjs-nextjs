import { LikeOutlined } from '@ant-design/icons';
import classNames from 'classnames';
import React from 'react';

import styles from './Styles.module.scss';

export default function LikeChat(props: Omit<React.HTMLAttributes<HTMLDivElement>, 'ref'>) {
  const { className, ...restProps } = props;

  return (
    <div
      className={classNames(styles['like-chat'], className)}
      {...restProps}
    >
      <LikeOutlined />
    </div>
  );
}
