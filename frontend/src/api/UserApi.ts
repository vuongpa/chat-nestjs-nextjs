import { User } from '@/models/userModel';
import { FormUtil } from '@/utils/form.util';
import RequestUtil from '@/utils/request.util';

const UserApi = {
  async updateUser(id?: string, data?: User) {
    const userUpdated = await RequestUtil.axios.put(`/users/${id}`, data);
    return userUpdated;
  },
  async searchUser(_q: string) {
    const filter = { _q };
    return RequestUtil.axios.get(FormUtil.endpointWithQuery(`/users/search?filter=${JSON.stringify(filter)}`));
  },
};
export default UserApi;
