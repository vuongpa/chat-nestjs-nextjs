import { createBrowserHistory } from 'history';

const HistoryUtil = {
  history: createBrowserHistory(),
};
export default HistoryUtil;
