import { Button, Form, Popover } from 'antd';
import React, { useEffect, useRef, useState } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import { SmileOutlined, FileImageOutlined, LinkOutlined } from '@ant-design/icons';
import EmojiPicker, { EmojiClickData } from 'emoji-picker-react';
import classNames from 'classnames';
import { useDispatch } from 'react-redux';
import { uuid } from 'uuidv4';
import { pick } from 'lodash';

import { chatActions } from '../chatSlice';
import { useActiveConversation } from '../chatHooks';

import styles from './Styles.module.scss';

import { useCurrentUser } from '@/features/auth/authHooks';
import { IMessage, MESSAGE_TYPE } from '@/models/messagesModel';

export default function InputChat() {
  const [typing, setTyping] = useState<boolean>(false);
  const [form] = Form.useForm();
  const inputRef = useRef<HTMLTextAreaElement>(null);
  const currentUser = useCurrentUser();
  const dispatch = useDispatch();
  const activeConversation = useActiveConversation();

  const handleTyping = () => {
    const text = form.getFieldValue('text');
    if (text && !typing) {
      setTyping(true);
    }

    if (!text && typing) {
      setTyping(false);
    }
  };

  const focusInput = () => inputRef.current?.focus();

  const focusInputAfterSendMessage = () => {
    form.setFieldValue('text', '');
    focusInput();
  };

  const onEmojiClick = (emojiData: EmojiClickData) => {
    const text = form.getFieldValue('text') || '';
    form.setFieldValue('text', `${text} ${emojiData.emoji}`);
    focusInput();
  };

  const handleSendMessage = () => {
    (window as any)?.scrollRef?.current?.scrollTo({ top: 0 });

    const activeConversationId = activeConversation?._id;
    if (!activeConversationId || !currentUser) {
      console.log('could not fould active conversation');
      return;
    }

    const message = form.getFieldValue('text');
    const fakeConversation = activeConversationId.includes('-') ? pick(activeConversation, ['users', '_id', 'fakeId']) : undefined;
    const fakeId = uuid();
    const fakeMessage: any = {
      _id: fakeId,
      fakeId,
      conversation_id: activeConversationId,
      fakeConversation,
      message,
      sender_id: currentUser,
      message_type: MESSAGE_TYPE.TEXT,
      sending: true,
    };

    dispatch(chatActions.sendMessage(fakeMessage));
    focusInputAfterSendMessage();
  };

  const handleInputKeyDown = (e: React.KeyboardEvent) => {
    if (e.code !== 'Enter') {
      return;
    }

    e.preventDefault();
    handleSendMessage();
  };

  useEffect(() => focusInput(), []);

  return (
    <div className={styles['input-chat']}>
      <div className={classNames(styles.header)}>
        <Popover
          trigger="click"
          arrow={false}
          content={(
            <EmojiPicker
              onEmojiClick={onEmojiClick}
              lazyLoadEmojis
              autoFocusSearch
              searchPlaceHolder="Tìm kiếm"
            />
        )}
        >
          <Button type="text">
            <SmileOutlined style={{ fontSize: 20 }} />
          </Button>
        </Popover>
        <Button type="text">
          <FileImageOutlined style={{ fontSize: 20 }} />
        </Button>
        <Button type="text">
          <LinkOutlined style={{ fontSize: 20 }} />
        </Button>
      </div>
      <div className={styles.input}>
        <Form form={form} name="input-chat">
          <Form.Item name="text">
            <TextareaAutosize
              className={styles['text-area-autosize']}
              onChange={handleTyping}
              rows={1}
              ref={inputRef}
              placeholder="@ Nhập tin nhắn"
              onKeyDown={handleInputKeyDown}
            />
          </Form.Item>
        </Form>
        <div className={styles.send}>
          <Button type="text" onClick={handleSendMessage}>
            GỬI
          </Button>
        </div>
      </div>
    </div>
  );
}
