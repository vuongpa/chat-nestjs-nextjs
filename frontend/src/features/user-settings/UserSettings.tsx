import { Tabs } from 'antd';

import UpdateUserInformation from './components/information/UpdateUserInformation';
import UpdatePassword from './components/password/UpdatePassword';

export default function UserSettings() {
  return (
    <Tabs
      defaultActiveKey="user-info"
      destroyInactiveTabPane
      items={[
        { label: 'Thông tin', key: 'user-info', children: <UpdateUserInformation /> },
        { label: 'Đổi mật khẩu', key: 'update-password', children: <UpdatePassword /> },
      ]}
    />
  );
}
