import {
  Form, Input, Button, Typography,
} from 'antd';
import Link from 'next/link';
import { useDispatch } from 'react-redux';
import { useCallback, useEffect } from 'react';
import { useRouter } from 'next/router';

import styles from '@/styles/Login.module.scss';
import { authActions } from '@/features/auth/authSlice';
import { AuthUtils } from '@/utils/auth.util';

export default function LoginPage() {
  const dispatch = useDispatch();

  const login = useCallback((formValue: any) => {
    dispatch(authActions.login(formValue));
  }, []);

  return (
    <div className={styles.login}>
      <Typography.Title className={styles.title} level={2}>Đăng nhập</Typography.Title>
      <Form
        name="login"
        onFinish={login}
        layout="vertical"
      >
        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              type: 'email',
              message: 'Vui lòng nhập Email chính xác',
            },
          ]}
        >
          <Input size="large" placeholder="* Email" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập mật khẩu!',
            },
            {
              min: 6,
              message: 'Mật khẩu phải có ít nhất 6 kí tự',
            },
          ]}
        >
          <Input.Password size="large" placeholder="* Mật khẩu" />
        </Form.Item>
        <Form.Item>
          <Button style={{ width: 350 }} size="large" type="primary" htmlType="submit">
            Đăng nhập
          </Button>
        </Form.Item>
        <div className={styles.start}>
          <Link href="/start">
            Tạo mới tại đây -
            {'>'}
          </Link>
        </div>
      </Form>
    </div>
  );
}
