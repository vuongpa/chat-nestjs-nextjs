import { IMessage } from './messagesModel';
import { User } from './userModel';

export interface Conversation {
  users: User[];
  messages: IMessage[];
  _id?: string;
  fakeId?: string;
  title?: string;
  background_url?: string;
  error: string;
  active: boolean;
  loaded: boolean;
  skip: number;
  limit: number;
  avatar_url?: string;
  // scrollHeight: number | null;
}

export interface IActiveConversation {
  conversationId: string;
  active: boolean;
}

export interface IRequestMessagesByConversation {
  conversation_id: string;
  error?: string;
  messages?: IMessage[];
  skip?: number;
  limit?: number;
}
