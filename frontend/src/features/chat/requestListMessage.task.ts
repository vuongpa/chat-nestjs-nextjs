import { call, put, select } from 'redux-saga/effects';
import { PayloadAction } from '@reduxjs/toolkit';

import { chatActions, chatSelector } from './chatSlice';

import ConversationApi from '@/api/ConversationApi';
import { Conversation, IRequestMessagesByConversation } from '@/models/conversationModel';
import { IMessage } from '@/models/messagesModel';

export default function* requestListMessage({ payload }: PayloadAction<IRequestMessagesByConversation>) {
  try {
    const listConversation: Conversation[] = yield select(chatSelector.listConversation);
    const oldListMessage: IMessage[] = listConversation.find((c) => c._id === payload.conversation_id)?.messages || [];

    const listMessage: IMessage[] = yield call(ConversationApi.getListMessage, payload.conversation_id, payload.skip, payload.limit);

    yield put(chatActions.requestListMessageSuccess({
      conversation_id: payload.conversation_id,
      messages: oldListMessage.length === 1 ? listMessage : [...listMessage, ...oldListMessage],
      skip: payload?.skip || 0,
    }));
  } catch (err: any) {
    yield put(chatActions.requestListMessageFail({
      conversation_id: payload.conversation_id,
      error: err?.response?.data?.message || '',
    }));
  }
}
