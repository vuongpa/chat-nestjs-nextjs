import { isEmpty } from 'lodash';
import { notification } from 'antd';

import { MessageUtil } from './message.util';
import RequestUtil from './request.util';

import { AnyObject } from '@/constants/common';

export interface FormProps {
  endpoint: string;
  method: 'GET' | 'POST' | 'DELETE' | 'PUT';
  modifyDataBeforeSubmit?: (formValue: AnyObject) => AnyObject;
  beforeSubmit?: (formValue: AnyObject) => boolean;
  onGotError?: (error: AnyObject) => void;
  onGotSuccess?: (response: AnyObject) => void;
  successMessage?: string;
  errorMessage?: string;
  notify?: boolean;
  params?: AnyObject;
}

export const FormUtil = {
  submitForm: (formProps: FormProps, submitData: AnyObject = {}) => {
    const {
      endpoint,
      method,
      modifyDataBeforeSubmit,
      beforeSubmit,
      onGotError,
      onGotSuccess,
      successMessage = MessageUtil.successMessage[method as keyof typeof MessageUtil.errorMessage],
      errorMessage,
      notify = false,
      params = {},
    } = formProps;

    const dataModifier = modifyDataBeforeSubmit?.(submitData) || submitData;
    if (beforeSubmit?.(dataModifier) ?? true) {
      RequestUtil.axios({
        method,
        data: dataModifier,
        url: FormUtil.endpointWithQuery(endpoint, params),
      })
        .then((response: any) => {
          if (notify) { notification.success({ message: successMessage }); }
          onGotSuccess?.(response);
          return response;
        })
        .catch((err: any) => {
          if (notify) {
            notification.error({ message: err?.response?.data?.message || errorMessage || MessageUtil.errorMessage[method as keyof typeof MessageUtil.errorMessage] });
          }
          onGotError?.(err);
          return err;
        });
    }
    return null;
  },
  endpointWithQuery: (endpoint: string, params?: AnyObject) => {
    if (isEmpty(params)) {
      return endpoint;
    }

    return `${endpoint}?filter=${encodeURIComponent(JSON.stringify(params))}`;
  },
};
