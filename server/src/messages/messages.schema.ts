import {
  AsyncModelFactory,
  Prop,
  Schema,
  SchemaFactory,
  getConnectionToken,
} from '@nestjs/mongoose';
import mongoose, { Connection } from 'mongoose';
import { Conversation } from 'src/conversation/conversation.schema';
import { User } from 'src/users/users.schema';

@Schema({
  timestamps: true,
  strict: false,
})
export class Message {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Conversation.name,
  })
  conversation_id: Conversation;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: User.name,
  })
  sender_id: User;

  @Prop()
  message_type: string;

  @Prop()
  message: string;

  @Prop()
  is_message_recall: boolean;

  @Prop()
  message_id: number;

  static TYPE = {
    IMAGE: 'image',
    TEXT: 'text',
    OTHER: 'other',
  };
}

const MessageSchema = SchemaFactory.createForClass(Message).index({
  message: 'text',
});

export const messageFactory: AsyncModelFactory = {
  name: Message.name,
  useFactory: (connection: Connection) => {
    MessageSchema.pre<Message>('save', async function () {
      const lastMessage = await connection.db
        .collection('messages')
        .findOne({}, { sort: { _id: -1 }, projection: { message_id: true } });
      const message_id = lastMessage?.message_id ? ++lastMessage.message_id : 1;
      this.message_id = message_id;
    });

    return MessageSchema;
  },
  inject: [getConnectionToken()],
};
