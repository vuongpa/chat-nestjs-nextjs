import { ModelDefinition, Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Message } from 'src/messages/messages.schema';

@Schema({ timestamps: true })
export class Attachment {
  @Prop()
  thumb_url: string;

  @Prop()
  file_url: string;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: Message.name,
  })
  messages_id: Message;
}

export const attachmentFactory: ModelDefinition = {
  name: Attachment.name,
  schema: SchemaFactory.createForClass(Attachment),
};
